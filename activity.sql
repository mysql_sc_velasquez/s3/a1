-- Insert Users
INSERT INTO users (email, password, datetime_created) VALUES(
	"johnsmith@gmail.com",
	"passwordA",
	CURRENT_TIMESTAMP
);
INSERT INTO users (email, password, datetime_created) VALUES(
	"juandelacruz@gmail.com",
	"passwordB",
	CURRENT_TIMESTAMP
);
INSERT INTO users (email, password, datetime_created) VALUES(
	"janesmith@gmail.com",
	"passwordC",
	CURRENT_TIMESTAMP
);
INSERT INTO users (email, password, datetime_created) VALUES(
	"mariadelacruz@gmail.com",
	"passwordD",
	CURRENT_TIMESTAMP
);
INSERT INTO users (email, password, datetime_created) VALUES(
	"johndoe@gmail.com",
	"passwordE",
	CURRENT_TIMESTAMP
);



-- Insert Post
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (
	1,
	"First Code",
	"Hello World",
	CURRENT_TIMESTAMP
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (
	1,
	"Second Code",
	"Hello Earth!",
	CURRENT_TIMESTAMP
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (
	2,
	"Third Code",
	"Welcome to Mars!",
	CURRENT_TIMESTAMP
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (
	4,
	"Fourth Code",
	"Bye bye solar system!",
	CURRENT_TIMESTAMP
);


-- Get all post with an author id 1
SELECT * FROM posts WHERE author_id = 1;

-- Get all the users email and datetime creation
SELECT email, datetime_created FROM users;

-- Update post content
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id =2;

-- DELETE user
DELETE FROM users WHERE email="johndoe@gmail.com";